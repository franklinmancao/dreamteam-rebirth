// Define player statistics
// Define sample player statistics
const team1Players = [
    {
        id: '1',
        name: 'Michael Jordan',
        position: 'SG',
        points: 30,
        fgm: 12,
        fga: 20,
        fgp: 60.0,
        ftm: 6,
        fta: 8,
        ftp: 75.0,
        tpm: 2,
        tpa: 4,
        tpp: 50.0,
        offReb: 2,
        defReb: 5,
        assists: 6,
        pFouls: 2,
        steals: 3,
        turnovers: 2,
        blocks: 1,
        plusMinus: 15
    },
    {
        id: '2',
        name: 'LeBron James',
        position: 'SF',
        points: 28,
        fgm: 11,
        fga: 18,
        fgp: 61.1,
        ftm: 5,
        fta: 7,
        ftp: 71.4,
        tpm: 1,
        tpa: 3,
        tpp: 33.3,
        offReb: 3,
        defReb: 6,
        assists: 7,
        pFouls: 3,
        steals: 2,
        turnovers: 3,
        blocks: 2,
        plusMinus: 12
    },
    {
        id: '3',
        name: 'Stephen Curry',
        position: 'PG',
        points: 32,
        fgm: 10,
        fga: 22,
        fgp: 45.5,
        ftm: 8,
        fta: 8,
        ftp: 100.0,
        tpm: 4,
        tpa: 10,
        tpp: 40.0,
        offReb: 1,
        defReb: 4,
        assists: 9,
        pFouls: 2,
        steals: 3,
        turnovers: 4,
        blocks: 0,
        plusMinus: 10
    },
    {
        id: '4',
        name: 'Kevin Durant',
        position: 'SF',
        points: 26,
        fgm: 9,
        fga: 20,
        fgp: 45.0,
        ftm: 7,
        fta: 8,
        ftp: 87.5,
        tpm: 1,
        tpa: 4,
        tpp: 25.0,
        offReb: 2,
        defReb: 5,
        assists: 5,
        pFouls: 3,
        steals: 1,
        turnovers: 2,
        blocks: 3,
        plusMinus: 8
    },
    {
        id: '5',
        name: 'Kawhi Leonard',
        position: 'SF',
        points: 24,
        fgm: 8,
        fga: 17,
        fgp: 47.1,
        ftm: 6,
        fta: 7,
        ftp: 85.7,
        tpm: 2,
        tpa: 5,
        tpp: 40.0,
        offReb: 2,
        defReb: 7,
        assists: 4,
        pFouls: 2,
        steals: 3,
        turnovers: 2,
        blocks: 2,
        plusMinus: 7
    },

];

const team2Players = [
    {
        id: '6',
        name: 'Giannis Antetokounmpo',
        position: 'PF',
        points: 30,
        fgm: 11,
        fga: 19,
        fgp: 57.9,
        ftm: 8,
        fta: 10,
        ftp: 80.0,
        tpm: 0,
        tpa: 1,
        tpp: 0.0,
        offReb: 4,
        defReb: 8,
        assists: 6,
        pFouls: 3,
        steals: 2,
        turnovers: 4,
        blocks: 3,
        plusMinus: 9
    },
    {
        id: '7',
        name: 'Anthony Davis',
        position: 'PF',
        points: 25,
        fgm: 10,
        fga: 18,
        fgp: 55.6,
        ftm: 5,
        fta: 6,
        ftp: 83.3,
        tpm: 0,
        tpa: 0,
        tpp: 0.0,
        offReb: 3,
        defReb: 6,
        assists: 3,
        pFouls: 2,
        steals: 2,
        turnovers: 3,
        blocks: 4,
        plusMinus: 10
    },
    {
        id: '8',
        name: 'James Harden',
        position: 'SG',
        points: 29,
        fgm: 8,
        fga: 20,
        fgp: 40.0,
        ftm: 11,
        fta: 13,
        ftp: 84.6,
        tpm: 2,
        tpa: 7,
        tpp: 28.6,
        offReb: 1,
        defReb: 5,
        assists: 10,
        pFouls: 3,
        steals: 4,
        turnovers: 5,
        blocks: 1,
        plusMinus: 8
    },
    {
        id: '9',
        name: 'Luka Dončić',
        position: 'SG',
        points: 27,
        fgm: 10,
        fga: 21,
        fgp: 47.6,
        ftm: 5,
        fta: 7,
        ftp: 71.4,
        tpm: 2,
        tpa: 6,
        tpp: 33.3,
        offReb: 2,
        defReb: 6,
        assists: 8,
        pFouls: 2,
        steals: 3,
        turnovers: 4,
        blocks: 1,
        plusMinus: 11
    },
    {
        id: '10',
        name: 'Damian Lillard',
        position: 'PG',
        points: 28,
        fgm: 9,
        fga: 20,
        fgp: 45.0,
        ftm: 7,
        fta: 8,
        ftp: 87.5,
        tpm: 3,
        tpa: 9,
        tpp: 33.3,
        offReb: 1,
        defReb: 4,
        assists: 9,
        pFouls: 2,
        steals: 2,
        turnovers: 3,
        blocks: 0,
        plusMinus: 9
    }
]

const gameParams = {
    duration: '4 quarters',
    quarterDuration: '12 minutes',
    possessionsPerQuarter: 20 // Adjust as needed
};

// Example function to display teams and game parameters
function displayGameInfo() {
    console.log('Team 1 Players:');
    console.log(team1Players);
    console.log('Team 2 Players:');
    console.log(team2Players);
    console.log('Game Parameters:');
    console.log(gameParams);
}

// Call the function to display game information
displayGameInfo();

// Sample code for simulating a possession in a basketball game

// Define game state
let gameState = {
    possession: 1, // Team 1 starts with the possession
    scoreTeam1: 0,
    scoreTeam2: 0,
    timeRemaining: 48, // 48 minutes in a standard game
    quarter: 1,
    possessionsLeft: 80 // 20 possessions per quarter for both teams
};

// Define functions for various actions
function calculateShotOutcome(player) {
    const shotOutcome = Math.random(); // Random number between 0 and 1
    if (shotOutcome < player.fgp / 100) {
        return true; // Successful shot
    } else {
        return false; // Missed shot
    }
}

function calculateFreeThrowOutcome(player) {
    const shotOutcome = Math.random(); // Random number between 0 and 1
    if (shotOutcome < player.ftp / 100) {
        return true; // Made free throw
    } else {
        return false; // Missed free throw
    }
}

function calculateThreePointOutcome(player) {
    const shotOutcome = Math.random(); // Random number between 0 and 1
    if (shotOutcome < player.tpp / 100) {
        return true; // Successful three-pointer
    } else {
        return false; // Missed three-pointer
    }
}

function simulateAssist(player, teammate) {
    // Simulate assist 
    console.log(`Assist made by ${player.name} to ${teammate.name}!`);
}

function simulateRebound(player) {
    const offensiveReboundChance = Math.random(); // Random number between 0 and 1
    const defensiveReboundChance = Math.random(); // Random number between 0 and 1

    if (offensiveReboundChance < player.offReb / 10) {
        console.log('Offensive rebound grabbed!');
    }

    if (defensiveReboundChance < player.defReb / 10) {
        console.log('Defensive rebound grabbed!');
    }
}

function simulateSteal(player) {
    const stealChance = Math.random(); // Random number between 0 and 1
    if (stealChance < player.steals / 10) {
        console.log('Steal made!');
    }
}

function simulateBlock(player) {
    const blockChance = Math.random(); // Random number between 0 and 1
    if (blockChance < player.blocks / 10) {
        console.log('Shot blocked!');
    }
}

function simulatePersonalFoul(player) {
    const foulChance = Math.random(); // Random number between 0 and 1
    if (foulChance < player.pFouls / 10) {
        console.log('Personal foul committed!');
    }
}

// Define simulatePossession function
function simulatePossession(player = null) {
    console.log(`Quarter ${gameState.quarter}, Ball Possession on Team ${gameState.possession} `);

    // Determine which team has the possession
    const currentTeam = (gameState.possession === 1) ? team1Players : team2Players;
    const opposingTeam = (gameState.possession === 1) ? team2Players : team1Players;

    var currentPlayer;
    var assistedPlayer;

    if (player) {
        //we already know who the player
        currentPlayer = player;
    } else {
        // Randomly select a player from the current team
        const randomPlayerIndex = Math.floor(Math.random() * currentTeam.length);
        currentPlayer = currentTeam[randomPlayerIndex];
    }


    console.log(`Current player: ${currentPlayer.name}`);

    // Simulate actions based on the player's role
    if (currentPlayer.position === 'SG' || currentPlayer.position === 'SF') { 
        // Simulate shooting
        if (calculateShotOutcome(currentPlayer)) {
            console.log('Shot made!');
            gameState.possession === 1 ? gameState.scoreTeam1 += 2 : gameState.scoreTeam2 += 2
        } else {
            console.log('Shot missed!');
        }

        gameState.possession = (gameState.possession === 1) ? 2 : 1; // Switch possession

    } else { 
        // Simulate passing
        const teammateIndex = Math.floor(Math.random() * currentTeam.length);
        const teammate = currentTeam[teammateIndex];
        simulateAssist(currentPlayer, teammate);
        assistedPlayer = teammate
    }

    // Update game state
    gameState.possessionsLeft--;

    // Check if the quarter is over
    if (gameState.possessionsLeft === 0) {
        gameState.quarter++;
        gameState.possessionsLeft = 20; // Reset possessions for the new quarter
    }

    // Check if the game is over
    if (gameState.quarter > 4) {
        console.log('Game over!');
        console.log(`Final Score - Team 1: ${gameState.scoreTeam1}, Team 2: ${gameState.scoreTeam2}`);
        return;
    }

    // Simulate next possession
    simulatePossession(assistedPlayer);
}

// Call the function to start the game simulation
simulatePossession();
